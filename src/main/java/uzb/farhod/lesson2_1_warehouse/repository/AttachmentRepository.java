package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson2_1_warehouse.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment,Integer> {
}
