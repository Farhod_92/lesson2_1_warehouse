package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Client;
import uzb.farhod.lesson2_1_warehouse.entity.OutputProduct;
import uzb.farhod.lesson2_1_warehouse.projection.ClientProjection;
import uzb.farhod.lesson2_1_warehouse.projection.OutputProductProjection;

@RepositoryRestResource(path = "outputProduct",collectionResourceRel = "list", excerptProjection = OutputProductProjection.class)
public interface OutputProductRepository extends JpaRepository<OutputProduct,Integer> {
}
