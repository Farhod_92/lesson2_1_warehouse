package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Input;
import uzb.farhod.lesson2_1_warehouse.projection.InputProjection;

@RepositoryRestResource(path = "input", collectionResourceRel = "list",excerptProjection = InputProjection.class)
public interface InputRepository extends JpaRepository<Input, Integer> {
}
