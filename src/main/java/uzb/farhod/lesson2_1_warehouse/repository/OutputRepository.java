package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Output;
import uzb.farhod.lesson2_1_warehouse.entity.Warehouse;
import uzb.farhod.lesson2_1_warehouse.projection.OutputProjection;
import uzb.farhod.lesson2_1_warehouse.projection.WarehouseProjection;

@RepositoryRestResource(path = "output", collectionResourceRel = "list",excerptProjection = OutputProjection.class)
public interface OutputRepository extends JpaRepository<Output, Integer> {
}
