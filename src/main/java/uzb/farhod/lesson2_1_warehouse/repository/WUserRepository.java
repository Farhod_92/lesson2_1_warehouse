package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Measurement;
import uzb.farhod.lesson2_1_warehouse.entity.WUser;
import uzb.farhod.lesson2_1_warehouse.projection.MeasurementProjection;
import uzb.farhod.lesson2_1_warehouse.projection.WUserProjection;

@RepositoryRestResource(path = "wuser", collectionResourceRel = "list",excerptProjection = WUserProjection.class)
public interface WUserRepository extends JpaRepository<WUser, Integer> {
}
