package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.InputProduct;
import uzb.farhod.lesson2_1_warehouse.entity.Warehouse;
import uzb.farhod.lesson2_1_warehouse.projection.InputProductProjection;
import uzb.farhod.lesson2_1_warehouse.projection.WarehouseProjection;

@RepositoryRestResource(path = "inputProduct", collectionResourceRel = "list",excerptProjection = InputProductProjection.class)
public interface InputProductRepository extends JpaRepository<InputProduct, Integer> {
}
