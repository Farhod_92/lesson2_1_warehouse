package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Client;
import uzb.farhod.lesson2_1_warehouse.entity.Currency;
import uzb.farhod.lesson2_1_warehouse.projection.ClientProjection;
import uzb.farhod.lesson2_1_warehouse.projection.CurrencyProjection;

@RepositoryRestResource(path = "currency",collectionResourceRel = "list", excerptProjection = CurrencyProjection.class)
public interface CurrencyRepository extends JpaRepository<Currency,Integer> {
}
