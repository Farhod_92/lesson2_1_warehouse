package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson2_1_warehouse.entity.Attachment;
import uzb.farhod.lesson2_1_warehouse.entity.AttachmentContent;

import java.util.Optional;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent,Integer> {
    Optional<AttachmentContent> findByAttachment(Attachment attachment);
}
