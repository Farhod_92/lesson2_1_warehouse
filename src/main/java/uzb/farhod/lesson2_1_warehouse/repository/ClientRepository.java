package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import uzb.farhod.lesson2_1_warehouse.entity.Client;
import uzb.farhod.lesson2_1_warehouse.projection.ClientProjection;

@RepositoryRestResource(path = "client",collectionResourceRel = "list", excerptProjection = ClientProjection.class)
public interface ClientRepository extends JpaRepository<Client,Integer> {
}
