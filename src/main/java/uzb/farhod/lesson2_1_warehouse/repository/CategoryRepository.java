package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Category;
import uzb.farhod.lesson2_1_warehouse.projection.CategoryProjection;

@RepositoryRestResource(path = "category", collectionResourceRel = "list", excerptProjection = CategoryProjection.class)
public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
