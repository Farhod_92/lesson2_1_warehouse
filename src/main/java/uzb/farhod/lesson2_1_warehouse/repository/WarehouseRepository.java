package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Input;
import uzb.farhod.lesson2_1_warehouse.entity.Warehouse;
import uzb.farhod.lesson2_1_warehouse.projection.InputProjection;
import uzb.farhod.lesson2_1_warehouse.projection.WarehouseProjection;

@RepositoryRestResource(path = "warehouse", collectionResourceRel = "list",excerptProjection = WarehouseProjection.class)
public interface WarehouseRepository extends JpaRepository<Warehouse, Integer> {
}
