package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Measurement;
import uzb.farhod.lesson2_1_warehouse.entity.Product;
import uzb.farhod.lesson2_1_warehouse.projection.MeasurementProjection;
import uzb.farhod.lesson2_1_warehouse.projection.ProductProjection;

@RepositoryRestResource(path = "product", collectionResourceRel = "list",excerptProjection = ProductProjection.class)
public interface ProductRepository extends JpaRepository<Product, Integer> {
}
