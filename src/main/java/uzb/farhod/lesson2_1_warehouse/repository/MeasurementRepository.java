package uzb.farhod.lesson2_1_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson2_1_warehouse.entity.Measurement;
import uzb.farhod.lesson2_1_warehouse.entity.Warehouse;
import uzb.farhod.lesson2_1_warehouse.projection.MeasurementProjection;
import uzb.farhod.lesson2_1_warehouse.projection.WarehouseProjection;

@RepositoryRestResource(path = "measurement", collectionResourceRel = "list",excerptProjection = MeasurementProjection.class)
public interface MeasurementRepository extends JpaRepository<Measurement, Integer> {
}
