package uzb.farhod.lesson2_1_warehouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson21WarehouseApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson21WarehouseApplication.class, args);
    }

}
