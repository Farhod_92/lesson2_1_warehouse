package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.Category;
import uzb.farhod.lesson2_1_warehouse.entity.Currency;

@Projection(types = Currency.class)
public interface CurrencyProjection {
    Integer getId();
    String getName();
    boolean getActive();
}
