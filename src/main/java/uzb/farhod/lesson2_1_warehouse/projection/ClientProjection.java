package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.Client;

@Projection(types = Client.class)
public interface ClientProjection {
    Integer getId();
    String getName();
    String getPhoneNumber();
}
