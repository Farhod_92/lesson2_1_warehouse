package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.*;

import java.util.Date;

@Projection(types = Output.class)
public interface OutputProjection {
    Integer getId();
    Date getDate();
    Warehouse getWarehouse();
    Client getClient();
    Currency getCurrency();
    Integer getFactureNumber();
    String getCode();
}
