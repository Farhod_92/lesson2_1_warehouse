package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.Input;
import uzb.farhod.lesson2_1_warehouse.entity.InputProduct;
import uzb.farhod.lesson2_1_warehouse.entity.Product;
import uzb.farhod.lesson2_1_warehouse.entity.Warehouse;

import javax.persistence.ManyToOne;
import java.util.Date;

@Projection(types = InputProduct.class)
public interface InputProductProjection {
    Integer getId();
    Integer getAmount();
    Float getPrice();
    Date getExpireDate();
    Product getProduct();
    Input getInput();
}
