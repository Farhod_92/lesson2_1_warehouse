package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.Measurement;
import uzb.farhod.lesson2_1_warehouse.entity.Warehouse;

@Projection(types = Measurement.class)
public interface MeasurementProjection {
    Integer getId();
    String getName();
    boolean getActive();
}
