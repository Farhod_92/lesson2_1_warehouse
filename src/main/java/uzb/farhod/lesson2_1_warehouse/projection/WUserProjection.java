package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.WUser;
import uzb.farhod.lesson2_1_warehouse.entity.Warehouse;

@Projection(types = WUser.class)
public interface WUserProjection {
    Integer getId();
    String getFirstName();
    String getLastName();
    String getPhoneNumber();
    boolean getActive();
    String getCode();
    String getPassword();
}
