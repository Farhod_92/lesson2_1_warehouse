package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.Supplier;
import uzb.farhod.lesson2_1_warehouse.entity.Warehouse;

@Projection(types = Supplier.class)
public interface SupplierProjection {
    Integer getId();
    String getName();
    boolean getActive();
    String getPhoneNumber();
}
