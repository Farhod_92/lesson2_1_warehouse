package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.*;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Projection(types = Product.class)
public interface ProductProjection {
    Integer getId();

    String ngetName();

    Category getCategory();
   Attachment getAttachment();

    String getCode();

    Measurement getMeasurement();

    boolean getActive();

    List<OutputProduct> getOutputProducts();

    List<InputProduct> getInputProducts();
}
