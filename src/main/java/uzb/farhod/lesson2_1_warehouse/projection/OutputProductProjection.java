package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.Input;
import uzb.farhod.lesson2_1_warehouse.entity.InputProduct;
import uzb.farhod.lesson2_1_warehouse.entity.OutputProduct;
import uzb.farhod.lesson2_1_warehouse.entity.Product;

import java.util.Date;

@Projection(types = OutputProduct.class)
public interface OutputProductProjection {
    Integer getId();
    Integer getAmount();
    Float getPrice();
    Product getProduct();
    Input getOutput();
}
