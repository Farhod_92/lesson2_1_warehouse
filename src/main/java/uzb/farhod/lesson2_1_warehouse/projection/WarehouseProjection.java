package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.*;

import java.util.Date;
import java.util.List;

@Projection(types = Warehouse.class)
public interface WarehouseProjection {
    Integer getId();
    String getName();
    boolean getActive();
}
