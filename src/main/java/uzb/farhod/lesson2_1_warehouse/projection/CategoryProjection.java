package uzb.farhod.lesson2_1_warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.farhod.lesson2_1_warehouse.entity.Category;

import javax.persistence.ManyToOne;

@Projection(types = Category.class)
public interface CategoryProjection {
    Integer getId();
    Integer getParentCategoryId();
    String getName();
    boolean getActive();
}
