package uzb.farhod.lesson2_1_warehouse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToOne
    private Category category;

    @ManyToOne
    @JoinColumn(name = "photo_id")
    private Attachment attachment;

    private String code;

    @ManyToOne
    private Measurement measurement;

    private boolean active;

    @OneToMany(mappedBy = "product")
    private List<OutputProduct> outputProducts;

    @OneToMany(mappedBy = "product")
    private List<InputProduct> inputProducts;
}
