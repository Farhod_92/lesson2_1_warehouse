package uzb.farhod.lesson2_1_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uzb.farhod.lesson2_1_warehouse.entity.Attachment;
import uzb.farhod.lesson2_1_warehouse.entity.AttachmentContent;
import uzb.farhod.lesson2_1_warehouse.repository.AttachmentContentRepository;
import uzb.farhod.lesson2_1_warehouse.repository.AttachmentRepository;

import java.util.Optional;

@Service
public class AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public boolean add( MultipartFile photo){
      try  {
            Attachment attachment = new Attachment();
            attachment.setContentType(photo.getContentType());
            attachment.setName(photo.getOriginalFilename());
            attachment.setSize(photo.getSize());
            Attachment savedAttachment = attachmentRepository.save(attachment);

            AttachmentContent attachmentContent = new AttachmentContent();
            attachmentContent.setAttachment(savedAttachment);
            attachmentContent.setBytes(photo.getBytes());
            attachmentContentRepository.save(attachmentContent);
            return true;
        }catch (Exception e){
          return false;
      }
    }

    public ResponseEntity<?> get(Integer id){
        Optional<Attachment> byId = attachmentRepository.findById(id);
        if(!byId.isPresent())
            return null;

        Attachment attachment = byId.get();
        Optional<AttachmentContent> byAttachment = attachmentContentRepository.findByAttachment(attachment);

        if(!byAttachment.isPresent())
            return null;

        AttachmentContent attachmentContent = byAttachment.get();

        return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getBytes());

    }

    public boolean delete(Integer id){
        Optional<Attachment> attachmentOptional = attachmentRepository.findById(id);
        if(!attachmentOptional.isPresent())
            return false;

        Attachment attachment = attachmentOptional.get();
        Optional<AttachmentContent> attachmentContentOptional = attachmentContentRepository.findByAttachment(attachment);
        if(!attachmentContentOptional.isPresent())
            return false;
        try{
         attachmentContentRepository.deleteById(attachmentContentOptional.get().getId());
         return true;
        }catch (Exception e){
            return false;
        }
    }
}
