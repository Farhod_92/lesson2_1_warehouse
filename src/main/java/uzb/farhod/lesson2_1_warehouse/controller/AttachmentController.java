package uzb.farhod.lesson2_1_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uzb.farhod.lesson2_1_warehouse.entity.Attachment;
import uzb.farhod.lesson2_1_warehouse.entity.AttachmentContent;
import uzb.farhod.lesson2_1_warehouse.service.AttachmentService;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {
    @Autowired
    AttachmentService attachmentService;

    @PostMapping
    public boolean add(@RequestParam("photo") MultipartFile photo){
       return attachmentService.add( photo);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> get(@PathVariable Integer id){
        return attachmentService.get(id);
    }

    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable Integer id){
        return attachmentService.delete(id);
    }

}
